Router.configure({
  layoutTemplate: 'layout'
});

Router.map(function() {
  this.route('main', {
    path: '/',
    layoutTemplate: 'anotherLayout'
  });
  this.route('test', {path: '/test'});
  this.route('items', {path: '/items'});
  this.route('users', {path: '/users'});
});

