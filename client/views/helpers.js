Template.items.helpers({
  items: function() {
    return Items.find();
  }
});

Template.main.helpers({
  title: function() {
    return Session.get("pageTitle");
  }
});
