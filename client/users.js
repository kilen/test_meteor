Template.users.events = {
  'click #submit': function(event, template) {
    var name = template.find("#name").value;
    var gender = template.find("#gender").value;
    Users.insert({ name: name, gender: gender });
  }
}

Template.users.helpers({
  users: function() {
    return Users.find();
  }
});

