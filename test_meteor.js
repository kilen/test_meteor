if (Meteor.isClient) {
  Meteor.subscribe("items");
  Meteor.subscribe("users");
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });

  Meteor.publish('items', function() {
    return Items.find();
  });
  Meteor.publish('users', function() {
    return Users.find();
  });
}
